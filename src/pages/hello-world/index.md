---
title: Hello World
date: '2018-10-30T23:58:03.284Z'
---

This is my first post on my new blog! I'm so exciting!

![Seattle](./seattle.jpeg)

I'm sure I'll write a lot more interesting things in the future. So soon! 🚀
