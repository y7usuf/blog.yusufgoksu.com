import React from 'react'

// Import typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

import profilePic from './profile-pic.jpg'
import { rhythm } from '../utils/typography'

const Bio = () => {
  return (
    <div
      style={{
        display: 'flex',
        marginBottom: rhythm(2.5),
      }}
    >
      <img
        src={profilePic}
        alt={`Yusuf E. Göksu`}
        style={{
          marginRight: rhythm(1 / 2),
          marginBottom: 0,
          width: rhythm(2),
          height: rhythm(2),
          borderRadius: 50,
        }}
      />
      <p>
        Software Engineer at <a href="https://www.scotty.app/">Scotty</a> • +4
        yrs Software Developer • 1 yr Software Engineer • Launched 1 failed
        startups • 🌇 🌃
        <a href="https://twitter.com/y7usuf">
          <strong>@y7usuf</strong>
        </a>
      </p>
    </div>
  )
}

export default Bio
